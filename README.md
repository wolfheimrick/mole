[![Snap Status](https://build.snapcraft.io/badge/wolfsubak/mole.svg)](https://build.snapcraft.io/user/wolfsubak/mole)

# mole

Server that will get datas (temperature and humidity) from esp32. Will be package as a snap.

```
port 8080
```

GET /entries
