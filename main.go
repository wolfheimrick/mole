package main

/*
test:
λ curl -X POST -d "hello world" http://127.0.0.1:3000/humidity
*/

import (
	"net/http"

	"github.com/go-martini/martini"
)

type Encoder interface {
	Encode(v ...interface{}) (string, error)
}

type EspEntry struct {
	Humidity int `json:"humidity"`
}

type DB interface {
	GetMode() string
	HumidityEntry(e EspEntry) int
}

func main() {
	m := martini.Classic()
	m.Get("/getmode", GetMode)
	m.Get("GetEntries", func() {
		println("not yet implemented")
	})
	m.Post("/humidity", HumidityEntry)
	m.Run()
}

func GetMode() string {
	return "0"
}

func HumidityEntry(enc Encoder, db DB, parms martini.Params) int {
	return http.StatusOK
}
